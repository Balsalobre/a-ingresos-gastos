## Integración Continua

[DEPENDENCIES, TESTING, BUILD, DEPLOY]

### 1 Configuración karma.conf.js

Se usa esta configuración por ser la más estándar, sin tener que ver con el proveedor (Configure CLI for CI testing in Chrome). Tenemos que entrar al fichero karma.conf.js y decirle que necesitamos un lanzador customizado. (Esto levanta un browser sin interfaz gráfica -- en background)

```
browsers: ['Chrome'],
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},
```
Podemos forzar el que no pase si la cobertura es inferior al 80%

- https://angular.io/guide/testing#code-coverage-enforcement

```
coverageIstanbulReporter: {
  reports: [ 'html', 'lcovonly' ],
  fixWebpackSourcePaths: true,
  thresholds: {
    statements: 80,
    lines: 80,
    branches: 80,
    functions: 80
  }
}
```

### 2 Configuración package.json

En el entorno de testing en el servidor de CI no necesitamos un watch, si un reporte de cobertura y el browser que hemos configurado. Se puede probar lanzando, npm run test:ci.

```
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```

### 3 Imagen version y pasos "stages"

```
image: node:12.16.1

stages:
  - install
  - test
  - build
  - deploy
```

### 4 Instalación de dependecias

Obtenemos como resultado final la instalación de las dependencias del proyecto en /node_modules, para que se pueda guardar para el siguiente stage podemos indicarle que es un artefacto, el siguiente proceso --> testing no tenga que volver a construir los node_modules.

Tambien podemos cachearlo para que cada vez que corra el install no inicie desde cero si no que tenga ya los node_modules en cache 

```
install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/

```
### 5 Lanzamos los tests

Ya pusimos que lanzase los test sin interfaz gráfica, pero hay que instalar chrome dentro del servidor de integración continua.

Lo primero a indicar es el ejecutable CHROME_BIN, y señalar que toda la parte de install en dependencies haya salido bien.

Antes de que corra cualquier script --> aquí es donde vamos a instalar Chrome

-[nota1]: Podemos tener una imagen de docker con google intalado para así agilizar este paso:
  test:
    image:

-[nota2]: Podemos incluir para el resultado una expresión regular --->  https://regex101.com/

![alt text](./src/assets/images/regExp.jpeg "Expresión regular")


```
tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
```

### 6 Build

Solo se va a hacer para la rama master.

Le indicamos con la variable que queremos el entorno de producción, y que depende del stage de instalación.
Es importante indicar que cuando acabe, el artefacto resultante está en dist/ que es donde angular hace la compilación de los ficheros.

La ruta de inicio se no está en / si no el /'nombre del proyecto/' quedaría así:

```
 "build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/a-ingresos-gastos/",
```

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master
```

### 7 Deploy

Para hacer un deploy en gitlab pages el job se tiene que llamar pages.

hacer un --> ng build --prod, para probar que directorio nos da dentro de dist

Si se hace una redirección que la mande al index ---> - cp public/index.html public/404.html


```
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/ingresoGastoApp/* ./public/
    - cp public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master
```

Si quisieramos hacer esto en un servidor privado, tendríamos que instalar en nuestro servidor --> gitlab runners
