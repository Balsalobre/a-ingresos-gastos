# NOTAS

## Variables HTML

Tenemos la referencia en el template [hero]="hero" --> en el hijo será hero mediante @Input

```
(ejemplo 1)
<form [formGroup]="registerForm" (ngSubmit)="createUser()">

(ejemplo 2)
<app-hero-child *ngFor="let hero of heroes"
  [hero]="hero"
  [master]="master">
</app-hero-child>
```

```
export class HeroChildComponent {
  @Input() hero: Hero;
  @Input('master') masterName: string;
}
```

## Deshabilitar un botón submit de un Reactive Forms

```
<form [formGroup]="registerForm" (ngSubmit)="createUser()">

  <button type="submit" [disabled]="registerForm.invalid" >Crear cuenta</button>
```

## Imprimir valor de la propiedad si existe dentro de un objeto

Si el valor de fuser fuese null el valor de la propiedad email nos daría undefined pero no nos mandaría un error.

```
console.log(fuser?.email);
```

## Implementar un guard de angular

Seleccionamos CanActivate para implentar esta interfaz

```
ng g guard services/auth
```

