# Apuntes RxJS

## 1 Ejemplo de uso tap:

Lo que hace tap es hacer un poco de espía de un observable, no es un observable en si mismo. Podemos suscribirnos a tap, pero tap en realidad no esta observando nada, tap está espiando alguna cosa que observa.

```
canActivate(): Observable<boolean> {
    return this.authService.isAuth()
      .pipe(
        tap(estado => {
          if (!estado) { this.router.navigate(['/login']); }
        })
      );
  }
```
