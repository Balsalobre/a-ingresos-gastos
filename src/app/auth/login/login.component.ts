import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import * as ui from '../../shared/ui.actions';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loading = false;
  loadingTemplate: TemplateRef<any>;
  loginForm: FormGroup;
  uiSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastrService: ToastrService,
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.uiSubscription = this.store.select('ui').subscribe(({isLoading}) => {
      this.loading = isLoading;
    });
  }

  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }

  async loginUser() {
    if (this.loginForm.invalid) { return; }
    try {
      // this.loading = true;
      this.store.dispatch(ui.isLoading());

      const user = await this.authService.loginUser(this.loginForm.value);
      if (user.operationType === 'signIn') {
        this.toastrService.success('Usuario y contraseña correctos', 'Login OK!');
      }
      // this.loading = false;
      this.store.dispatch(ui.stopLoading());

      this.router.navigate(['/']);
    } catch (e) {
      // this.loading = false;
      this.store.dispatch(ui.stopLoading());
      this.toastrService.error(e.message, 'Error!');
    }
  }
}
