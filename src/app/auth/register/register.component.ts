import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import * as ui from '../../shared/ui.actions';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  loading = false;
  loadingTemplate: TemplateRef<any>;
  registerForm: FormGroup;
  uiSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastrService: ToastrService,
    private store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.uiSubscription = this.store.select('ui').subscribe(storeUi => {
      this.loading = storeUi.isLoading;
      console.log('cargando subs');
    });
  }

  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }

  async createUser() {
    if (this.registerForm.invalid) { return; }
    try {
      // this.loading = true;
      this.store.dispatch(ui.isLoading());

      const user = await this.authService.createUser(this.registerForm.value);
      if (user.uid) {
        this.toastrService.success('Usuario registrado correctamente', 'Registro OK!');
      }
      // this.loading = false;
      this.store.dispatch(ui.stopLoading());

      this.router.navigate(['/']);
    } catch (e) {
      // this.loading = false;
      this.store.dispatch(ui.stopLoading());
      this.toastrService.error(e.message, 'Error!');
    }
  }
}
