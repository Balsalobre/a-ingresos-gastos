import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { IncomeExpenseService } from '../services/income-expense.service';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as icomeExpenseActions from '../income-expenses/income-expense.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  private userSubs: Subscription;
  private incomeExpenseSubs: Subscription;

  constructor(
    private store: Store<AppState>,
    private incomeExpenseService: IncomeExpenseService
    ) { }

  ngOnInit(): void {
    this.userSubs = this.store.select('auth')
      .pipe(filter( auth => auth.user !== null ))
      .subscribe(({ user }) => {
        this.incomeExpenseSubs = this.incomeExpenseService.incomesExpensesListener(user.uid)
          .subscribe( incomesExpensesFB => {
            // Lectura de incomes Expenses desde firebase al store
            this.store.dispatch(icomeExpenseActions.setItems({ items: incomesExpensesFB }));

          });
      });
  }

  ngOnDestroy(): void {
    this.userSubs.unsubscribe();
    this.incomeExpenseSubs.unsubscribe();
  }
}
