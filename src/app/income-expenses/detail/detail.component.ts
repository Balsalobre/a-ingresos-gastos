import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import IncomeExpense from 'src/app/models/income-expense.model';
import { IncomeExpenseService } from 'src/app/services/income-expense.service';

import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnDestroy {

  incomesExpenses: IncomeExpense[] = [];
  private incomesExpensesSubs: Subscription;

  constructor(
    private store: Store<AppState>,
    private incomeExpenseService: IncomeExpenseService,
    private firestore: AngularFirestore,
    private toastrService: ToastrService,
  ) { }

  ngOnInit(): void {
    this.incomesExpensesSubs = this.store.select('incomesExpenses')
    .subscribe(({ items })  => {
      this.incomesExpenses = items;
    });
  }

  async borrar(description: string, uid: string){
    try{
      await this.incomeExpenseService.removeIncomeExpense(uid);
      this.toastrService.success(`Item: ${description}`, 'Registro BORRADO!');
    } catch(e) {
      this.toastrService.error(`Error al borrar el registro ${uid}`, 'Error de borrado!');
      console.error(e);
    }
  }

  ngOnDestroy(): void {
    this.incomesExpensesSubs.unsubscribe();
  }
}
