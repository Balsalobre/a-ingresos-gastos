import { createReducer, on } from '@ngrx/store';
import IncomeExpense from '../models/income-expense.model';
import { setItems, unSetItems } from './income-expense.actions';

export interface State {
    items: IncomeExpense[];
}

export const initialState: State = {
   items: [],
}

const _incomeExpenseReducer = createReducer(initialState,

  on(setItems, (state, { items }) => ({ ...state, items: [ ...items ]})),
  on(unSetItems, state => ({ ...state, items: []})),

);

export function incomeExpenseReducer(state, action) {
    return _incomeExpenseReducer(state, action);
}
