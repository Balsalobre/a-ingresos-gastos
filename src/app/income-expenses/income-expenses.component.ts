import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { IncomeExpenseService } from '../services/income-expense.service';
import IncomeExpense from '../models/income-expense.model';

import { DocumentReference } from '@angular/fire/firestore';

import { Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as ui from '../shared/ui.actions';

@Component({
  selector: 'app-income-expenses',
  templateUrl: './income-expenses.component.html',
  styleUrls: ['./income-expenses.component.scss']
})
export class IncomeExpensesComponent implements OnInit, OnDestroy {

  incomeExpenseForm: FormGroup;
  loading = false;
  loadingTemplate: TemplateRef<any>;
  uiSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private incomeExpenseService: IncomeExpenseService,
    private toastrService: ToastrService,
    private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.incomeExpenseForm = this.fb.group({
      description: ['', Validators.required ],
      type: ['income', Validators.required],
      amount: ['', Validators.required ],
    });

    this.uiSubscription = this.store.select('ui').subscribe(({isLoading}) => {
      this.loading = isLoading;
    });
  }

  async save() {
    if(this.incomeExpenseForm.invalid) return;
    try {
      this.store.dispatch(ui.isLoading());
      const { description } = this.incomeExpenseForm.value;
      const incomeExpenseValue = { ...this.incomeExpenseForm.value, date: new Date().toString() }
      const save: DocumentReference = await this.incomeExpenseService.createIncomeExpense(incomeExpenseValue as IncomeExpense);
      if(save.id) {
        this.store.dispatch(ui.stopLoading());
        this.incomeExpenseForm.reset();
        this.toastrService.success(`Registro ${description}`, `Creado correctamente`);
      }
    } catch (e) {
      this.store.dispatch(ui.stopLoading());
      this.toastrService.success(`Error en la creación del registro`);
    }
  }

  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }
}
