import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { IncomeExpensesComponent } from './income-expenses.component';
import { StatisticComponent } from './statistic/statistic.component';
import { DetailComponent } from './detail/detail.component';
import { SortIncomesExpensesPipe } from '../pipes/sort-incomes-expenses.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutesModule } from '../dashboard/dashboard-routes.module';



@NgModule({
  declarations: [
    DashboardComponent,
    IncomeExpensesComponent,
    StatisticComponent,
    DetailComponent,
    SortIncomesExpensesPipe],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    ChartsModule,
    DashboardRoutesModule,
    NgxLoadingModule.forRoot({
      animationType: ngxLoadingAnimationTypes.cubeGrid,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)',
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff',
      secondaryColour: '#ffffff',
      tertiaryColour: '#ffffff'
    }),
  ]
})
export class IncomesExpensesModule { }
