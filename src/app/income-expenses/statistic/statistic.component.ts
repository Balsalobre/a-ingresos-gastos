import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import IncomeExpense from 'src/app/models/income-expense.model';

import { MultiDataSet, Label } from 'ng2-charts';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {

  incomesPercent: number = 0;
  expensesPercent: number = 0;

  totalIncomes: number = 0;
  totalExpenses: number = 0;

  doughnutChartLabels: Label[] = ['Gastos', 'Ingresos'];
  doughnutChartData: MultiDataSet = [[]];

  constructor(
    private store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    this.store.select('incomesExpenses').subscribe(
      ({ items }) =>  this.generarEstadistica(items));
  }

  generarEstadistica(items: IncomeExpense[]) {
    const total = items.reduce((acc, current) => acc + current.amount, 0);
    this.totalIncomes = items.filter(x => x.type === 'income').reduce((acc, curr) => acc + curr.amount, 0);
    this.totalExpenses= items.filter(x => x.type === 'expense').reduce((acc, curr) => acc + curr.amount, 0);
    this.incomesPercent = this.totalIncomes / total;
    this.expensesPercent = this.totalExpenses / total;

    this.doughnutChartData = [[ this.totalExpenses, this.totalIncomes ]]
  }
}
