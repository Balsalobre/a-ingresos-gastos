export default interface IncomeExpense {
  description: string;
  amount: number;
  type: string;
  uid?: string;
  date?: string;
}
