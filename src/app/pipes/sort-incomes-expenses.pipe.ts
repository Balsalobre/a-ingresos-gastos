import { Pipe, PipeTransform } from '@angular/core';
import IncomeExpense from '../models/income-expense.model';

@Pipe({
  name: 'sortIncomesExpenses'
})
export class SortIncomesExpensesPipe implements PipeTransform {

  transform(items: IncomeExpense[]): IncomeExpense[] {
    // Vale cualquiera de las dos opciones.
    // return [...items].sort((a, b) =>  new Date(a.date).getTime() - new Date(b.date).getTime());
    return items.slice().sort((a, b) =>  new Date(a.date).getTime() - new Date(b.date).getTime());
  }

}
