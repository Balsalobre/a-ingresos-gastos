import { Injectable } from '@angular/core';

import 'firebase/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as authActions from '../auth/auth.actions';
import * as icomeExpenseActions from '../income-expenses/income-expense.actions';

import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import User from '../models/user.interfacel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userSubscription: Subscription;
  private _user: User;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private store: Store<AppState>
  ) { }

  get user() {
    return this._user;
  }

  initAuthListener() {
    this.angularFireAuth.authState.subscribe(fuser => {
      // console.log('Usuario completo:', fuser);
      // console.log('Id:', fuser?.uid);
      // console.log('Email:', fuser?.email);
      if(fuser) {
        this.userSubscription = this.firestore.doc(`${fuser.uid}/usuario`).valueChanges().subscribe((firestoreUser: User) => {
          this._user = firestoreUser;
          this.store.dispatch(authActions.setUser({ user: firestoreUser }));
        });
      } else {
        this._user = null;
        this.userSubscription?.unsubscribe();
        this.store.dispatch(authActions.unSetUser());
        this.store.dispatch(icomeExpenseActions.unSetItems());
      }
    });
  }

  async createUser({name, email, password}: User): Promise<User> {
    const { user } =  await this.angularFireAuth.createUserWithEmailAndPassword(email, password);
    const newUser: User = {
      name,
      uid: user.uid,
      email: user.email
    };
    this.firestore.doc(`${user.uid}/usuario`).set(newUser);
    return newUser;
  }

  loginUser({email, password}: User): Promise<firebase.auth.UserCredential> {
    return this.angularFireAuth.signInWithEmailAndPassword(email, password);
  }

  logout(): Promise<void> {
    return this.angularFireAuth.signOut();
  }

  isAuth(): Observable<boolean> {
    return this.angularFireAuth.authState.pipe(
      map(fbuser => fbuser != null)
    );
  }
}
