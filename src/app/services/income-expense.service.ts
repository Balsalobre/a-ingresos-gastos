import { Injectable } from '@angular/core';

import 'firebase/firestore';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';

import IncomeExpense from '../models/income-expense.model';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class IncomeExpenseService {
  constructor(
    private fireStore: AngularFirestore,
    private authService: AuthService
  ) {}

  async createIncomeExpense(incomeExpense: IncomeExpense): Promise<DocumentReference> {
    try {
      return await this.fireStore
        .doc(`${this.authService.user.uid}/incomes-expenses`)
        .collection('items')
        .add(incomeExpense);
    } catch (e) {
      console.error('Error al crear IG desde el servicio', e);
    }
  }

  incomesExpensesListener(uid: string): Observable<any[]> {
    return this.fireStore.collection(`${uid}/incomes-expenses/items`)
    .snapshotChanges()
    .pipe(
      map(snapshot => snapshot.map(doc => ({
          uid: doc.payload.doc.id,
          ...doc.payload.doc.data() as any
      })))
    )
  }

  removeIncomeExpense(uidItem: string): Promise<void> {
    const userUid = this.authService.user.uid;
    return this.fireStore.doc(`${userUid}/incomes-expenses/items/${uidItem}`).delete();
  }
}
