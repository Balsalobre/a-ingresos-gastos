// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBddNuHG1TUEBUJMR8lTlpx2dh6ASzS5d4',
    authDomain: 'ingresos-gastos-app-70703.firebaseapp.com',
    databaseURL: 'https://ingresos-gastos-app-70703.firebaseio.com',
    projectId: 'ingresos-gastos-app-70703',
    storageBucket: 'ingresos-gastos-app-70703.appspot.com',
    messagingSenderId: '364228112840',
    appId: '1:364228112840:web:0571b4c78172814e4b50fd'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
